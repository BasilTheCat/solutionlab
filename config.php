<?php

$_SETTINGS['db'] = [
    
    'host'  => 'localhost',
    'user'  => 'root',
    'pass'  => '',
    'name'  => 'az_solutionlab'
];

$_SITE['path']['root']  = $_SERVER['DOCUMENT_ROOT'] . '/solutionlab/';
$_SITE['path']['logs']  = $_SITE['path']['root'] . 'logs/';

require_once( 'libs/MySQLi.php' );

$mysqli = new MySQLiWrapper( $_SETTINGS['db']['host'], $_SETTINGS['db']['user'], $_SETTINGS['db']['pass'], $_SETTINGS['db']['name'] );