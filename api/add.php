<?php

require( '../config.php' );

$result = [
    'success'   => false
];

if ( isset( $_POST['title'] ) && isset( $_POST['price'] ) && isset( $_POST['img'] ) ) {
    
    $sql = "
        INSERT
        INTO    t_coffees
        SET     title   = '" . $mysqli->esc( $_POST['title'] ) . "'
                , price = '" . floatval( $_POST['price'] ) . "'
                , img   = '" . $mysqli->esc( $_POST['img'] ) . "'
    ";
    
    if ( $mysqli->qry( $sql ) ) {
        
        $result['success'] = true;
    }
}

$mysqli->safe_close();

print json_encode( $result );
