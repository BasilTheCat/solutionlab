<?php

require( '../config.php' );

$sql = "
    SELECT  *
    FROM    t_coffees
    ORDER BY
            price ASC
";

$query = $mysqli->qry( $sql );

$result = [
    'success'   => false,
    'data'      => []
];

if ( $query && $query->num_rows > 0 ) {
    
    while( $rs = $query->fetch_assoc() ) {
        
        $result['data'][] = $rs;
    }
}

if ( $query || !empty( $result['data'] ) ) {
    
    $result['success'] = true;
}

$mysqli->safe_close();

print json_encode( $result );
