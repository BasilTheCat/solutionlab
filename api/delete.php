<?php

require( '../config.php' );

$result = [
    'success'   => false
];

if ( isset( $_POST['id'] ) ) {
    
    if ( $id = intval( $_POST['id'] ) ) {
        
        $sql = "
            DELETE
            FROM    t_coffees
            WHERE   id = " . $id . "
        ";
        
        if ( $mysqli->qry( $sql ) ) {
            
            $result['success'] = true;
        }
    }
}

$mysqli->safe_close();

print json_encode( $result );
