// ReactJS part
// Generate Billboard
class Billboard extends React.Component {
    
    componentDidMount() {
        
        CoffeeBillboard.init.bindDeletion();
    }
    
    componentDidUpdate() {
        
        CoffeeBillboard.init.bindDeletion();
    }
    
    render() {
        return(
            <span>
            {this.props.data.map(function(obj){
                return <CoffeeItem key={obj.id} data={obj} />
            })}
            </span>
        );
    }
}

// Generate Coffee Item
class CoffeeItem extends React.Component {
    
    render() {
        
        return (
            <div className="coffee-item" id={this.props.data.id}>
                <a className="delete" data-delete={this.props.data.id}></a>
                <div className="price">{this.props.data.price} &euro;</div>
                <div className="img">
                    <div className="img-container" style={{backgroundImage: 'url(' + this.props.data.img + ')'}}></div>
                </div>
                <div className="title">{this.props.data.title}</div>
            </div>
        );
    }
};

// jQuery part
$(function(){
    
    // prepare and rended billboard on load
    CoffeeBillboard.init.prepareAndRender();
});

// prepare class for billboard
var CoffeeBillboard = {
    
    // set urls
    url: {
        'list'  : '/solutionlab/api/list.php',
        'del'   : '/solutionlab/api/delete.php',
        'add'   : '/solutionlab/api/add.php'
    },
    
    // define data array
    data: [],
    
    // initiation
    init: {
        
        // prepare data and render billboard
        prepareAndRender() {
            
            CoffeeBillboard.init.getData();
            // render is called from inside the getData function
        },
        
        // getting data
        getData: function() {
            
            $.ajax({
                
                url: CoffeeBillboard.url.list,
                dataType: 'json',
                success: function( data ) {
                    
                    if ( data.success ) {
                        
                        CoffeeBillboard.data = data.data;
                        CoffeeBillboard.init.render();
                    }
                },
                error: function() {
                    
                    console.warn( 'Some error.' );
                }
            });
        },
        
        // render billboard
        render: function() {
            
            ReactDOM.render(
                <Billboard data={CoffeeBillboard.data} />,
                document.getElementById('items-holder')
            );
        },
        
        // bind deletion to the delete buttons
        bindDeletion: function() {
            
            $('#items-holder a.delete').each(function(){
                
                $(this).unbind('click').on('click', function(){
                
                    CoffeeBillboard.actions.deleteItem( $(this).attr('data-delete') );
                });
                
            });
        }
    },
    
    // actions
    actions: {
        
        // delete item
        deleteItem: function( id ) {
            
            $.ajax({
                
                url: CoffeeBillboard.url.del,
                type: 'post',
                dataType: 'json',
                data: 'id=' + id,
                success: function( data ) {
                    
                    if ( data.success ) {
                        
                        CoffeeBillboard.init.prepareAndRender();
                    }
                },
                error: function() {
                    
                    console.warn( 'Some error.' );
                }
            });
        },
        
        // add item
        addItem: function() {
            
            var data = $('#frm').serialize();
            $('#frm')[0].reset();
            console.log( data );
            console.log('add item');
            
            $.ajax({
                
                url: CoffeeBillboard.url.add,
                dataType: 'json',
                data: data,
                type: 'post',
                success: function( data ) {
                    
                    if ( data.success ) {
                        
                        CoffeeBillboard.init.prepareAndRender();
                        $('#form-modal').modal('toggle');
                    }
                },
                error: function() {

                    $('#form-modal').modal('toggle');
                    console.warn( 'Some error.' );
                }
            });
            
            return false;
        }
    }
}