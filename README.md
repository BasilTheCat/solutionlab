# Test task for Solutionlab

## Some notes
 1. Project is written without any frameworks.
 2. Clone project to `{webroot}/solutionlab` directory.
 3. Create `az_solutionlab` database and import `az_solutionlab.sql` dump there (you can find it in the core directory of the project).
 4. Configure database connections in `/config.php` file.
 5. Configure AJAX urls in the connections in the `/assets/js/billboard.js` file, if needed. Lines 56-58.
 6. Add writable `/logs` directory.

## Manual
For this manual we assume, that you've put this project into the `{webroot}/solutionlab/` directory, so it should be accessible through `http://localhost/solutionlab` URL. The interface is intuitive and fits the description of the task provided.

### Other notes
1. We use ReactJS only for to (re)render the active part of the page.
2. We use jQuery for all other tasks.
3. Bootstrap v4-alpha is used as main design framework.
4. Original CSS file is coded in Sass, so you will need Sass compiler in case you decide to make changes.