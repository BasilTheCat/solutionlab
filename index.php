<!DOCTYPE HTML>
<html>
    <head>
        <title>Solutionlab test</title>
        
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
        <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
        
        <script src="//unpkg.com/react@latest/dist/react.min.js"></script>
        <script src="//unpkg.com/react-dom@latest/dist/react-dom.min.js"></script>
        <script src="//unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
        
        <link rel="stylesheet" href="assets/css/billboard.min.css?<?php print( time() ); ?>">
        <script type="text/babel" src="assets/js/billboard.js?<?php print( time() ); ?>"></script>
        
    </head>
    <body>
        
        <div class="container-fluid">
            
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#form-modal">
                + Add Item
            </button>
            
            <div class="clearfix" style="height: 20px;"></div>
            
            <div id="billboard">
                
                <div id="items-holder" class="clearfix"></div>
            </div>
        
        </div>
        
        <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <form method="post" id="frm" onsubmit="return CoffeeBillboard.actions.addItem();">
                  <div class="modal-body">
                    <div class="form-group">
                        <input type="text" name="title" id="title" class="form-control" placeholder="Coffee title" required="true" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="price" id="price" class="form-control" placeholder="Price" required="true" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="img" id="img" class="form-control" placeholder="URL to the image" required="true" />
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="frm-submit">Save changes</button>
                  </div>
                </form>
            </div>
          </div>
        </div>
        
    </body>
</html>