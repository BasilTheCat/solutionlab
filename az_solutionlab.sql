-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.25-MariaDB - mariadb.org binary distribution
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных az_solutionlab
CREATE DATABASE IF NOT EXISTS `az_solutionlab` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `az_solutionlab`;

-- Дамп структуры для таблица az_solutionlab.t_coffees
CREATE TABLE IF NOT EXISTS `t_coffees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `img` tinytext NOT NULL COMMENT 'path to the image (or url)',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `price` (`price`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы az_solutionlab.t_coffees: ~3 rows (приблизительно)
DELETE FROM `t_coffees`;
/*!40000 ALTER TABLE `t_coffees` DISABLE KEYS */;
INSERT INTO `t_coffees` (`id`, `title`, `price`, `img`, `date_created`) VALUES
	(22, 'Coffee', 20.00, 'https://s-media-cache-ak0.pinimg.com/736x/87/d9/fb/87d9fb33b4019527a6088d56f3a1e46f--coffee-art-cup-of-coffee.jpg', '2017-08-06 17:11:48');
/*!40000 ALTER TABLE `t_coffees` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
