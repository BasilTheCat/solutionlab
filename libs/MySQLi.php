<?php

class MySQLiWrapper extends mysqli {
    
    // constructor
    public function __construct( $host, $user, $pass, $db ) {
		
        parent::__construct( $host, $user, $pass, $db );
        
        if( mysqli_connect_error() ) {
			
            die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        
        $this->qry("SET NAMES 'utf8' COLLATE 'utf8_general_ci';");
    }
    
    // execute query
    public function qry( $sql ) {
		
    	global $_SITE;
	
        $query = $this->query($sql);

    	// if query fails - write log
    	if ( !$query ) {
	    
            $filename = date('Ym') . '_sql.txt';
	    
			// write log
			$fh = fopen($_SITE['path']['root'] . $_SITE['path']['logs'] . $filename, 'a');
			$str_to_write = date("Y-m-d H:i:s") . "\n" . $_SERVER['SCRIPT_FILENAME'] . "\n" . $sql . "\n" . $this->error . "\r\n\r\n-------------------------\r\n\r\n";
			
			fwrite($fh, $str_to_write);
			fclose($fh);
		
			return false;
		} else {
	    
            return $query;
        }
    	
    }
    
    // get last executed query error
    public function getError() {
		
        $error = 'Statement failed ' . $this->errno . ': ' . $this->sqlstate . ' ' . $this->error;
        return $error;
    }
    
    // shortener for real_escape_string
    public function esc($value) {
		
        return $this->real_escape_string($value);
    }
    
    // safe close (with thread killing)
    public function safe_close() {
		
        $thread_id = $this->thread_id;
        $this->kill($thread_id);
        $this->close();
    }
}
